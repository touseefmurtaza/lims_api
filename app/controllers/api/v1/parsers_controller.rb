class Api::V1::ParsersController < ApplicationController
	skip_before_action :verify_authenticity_token
  require 'open-uri'
  require 'nokogiri'

  def parse_url
  	data = params[:link]
    if data
      html_doc = Nokogiri::HTML(open(data))
      h1_array = []
      h2_array = []
      h3_array = []
      link_array = []

      h1_nodes = html_doc.search("//h1")
      h1_nodes.each do |node|
        h1_array.push node.to_s.gsub("<h1>", "").gsub("</h1>", "")
      end

      h2_nodes = html_doc.search("//h2")
      h2_nodes.each do |node|
        h2_array.push node.to_s.gsub("<h2>", "").gsub("</h2>", "")
      end

      h3_nodes = html_doc.search("//h3")
      h3_nodes.each do |node|
        h3_array.push node.to_s.gsub("<h3>", "").gsub("</h3>", "")
      end

      link_nodes = html_doc.css('a')
      link_nodes.each do |node|
        link_array.push node["href"].to_s
      end
      parser = Parser.new(h1: h1_array, h2: h2_array, h3: h3_array, link: link_array)
      if parser.save!
        render :json => {success: true, data: parser.to_json}
      else
        render :json => {success: false, message: "Something Went Wrong."}
      end
    end
  end
end
