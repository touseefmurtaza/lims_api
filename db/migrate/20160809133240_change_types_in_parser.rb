class ChangeTypesInParser < ActiveRecord::Migration
  def change
  	change_column :parsers, :h1, :text, :limit => 4294967295
  	change_column :parsers, :h2, :text, :limit => 4294967295
  	change_column :parsers, :h3, :text, :limit => 4294967295
  	change_column :parsers, :link, :text, :limit => 4294967295
  end
end
