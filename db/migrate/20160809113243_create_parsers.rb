class CreateParsers < ActiveRecord::Migration
  def change
    create_table :parsers do |t|
      t.string :h1
      t.string :h2
      t.string :h3
      t.string :link

      t.timestamps null: false
    end
  end
end
